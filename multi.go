/*
	Go Logging provides common logging interfaces.
	Copyright 2019 The Go Logging Authors.

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// multi.go: a fan-out logger

package logging

func All(l ...SimpleLogger) MultiLogger {
	all := make(MultiLogger, 0, len(l))

	for _, l := range l {
		switch l := l.(type) {
		case MultiLogger:
			all = append(all, l...)

		case Logger:
			all = append(all, l)

		default:
			all = append(all, Polyfill(l))
		}
	}

	return all
}

type MultiLogger []Logger

func (l MultiLogger) each(do func(Logger)) {
	for _, l := range l {
		do(l)
	}
}

func (l MultiLogger) eachv(do func(Logger, ...interface{}), v ...interface{}) {
	l.each(func(l Logger) { do(l, v...) })
}

func (l MultiLogger) eachf(do func(Logger, string, ...interface{}), format string, v ...interface{}) {
	l.each(func(l Logger) { do(l, format, v...) })
}

func (l MultiLogger) eachd(do func(Logger, int, ...interface{}), depth int, v ...interface{}) {
	l.each(func(l Logger) { do(l, depth, v...) })
}

func (l MultiLogger) SetFlags(flag int)                    { l.each(func(l Logger) { l.SetFlags(flag) }) }
func (l MultiLogger) Error(v ...interface{})               { l.eachv(Logger.Error, v...) }
func (l MultiLogger) Fatal(v ...interface{})               { l.eachv(Logger.Fatal, v...) }
func (l MultiLogger) Info(v ...interface{})                { l.eachv(Logger.Info, v...) }
func (l MultiLogger) Warning(v ...interface{})             { l.eachv(Logger.Warning, v...) }
func (l MultiLogger) Errorf(f string, v ...interface{})    { l.eachf(Logger.Errorf, f, v...) }
func (l MultiLogger) Fatalf(f string, v ...interface{})    { l.eachf(Logger.Fatalf, f, v...) }
func (l MultiLogger) Infof(f string, v ...interface{})     { l.eachf(Logger.Infof, f, v...) }
func (l MultiLogger) Warningf(f string, v ...interface{})  { l.eachf(Logger.Warningf, f, v...) }
func (l MultiLogger) ErrorDepth(d int, v ...interface{})   { l.eachd(Logger.ErrorDepth, d, v...) }
func (l MultiLogger) FatalDepth(d int, v ...interface{})   { l.eachd(Logger.FatalDepth, d, v...) }
func (l MultiLogger) InfoDepth(d int, v ...interface{})    { l.eachd(Logger.InfoDepth, d, v...) }
func (l MultiLogger) WarningDepth(d int, v ...interface{}) { l.eachd(Logger.WarningDepth, d, v...) }
func (l MultiLogger) Errorln(v ...interface{})             { l.eachv(Logger.Errorln, v...) }
func (l MultiLogger) Fatalln(v ...interface{})             { l.eachv(Logger.Fatalln, v...) }
func (l MultiLogger) Infoln(v ...interface{})              { l.eachv(Logger.Infoln, v...) }
func (l MultiLogger) Warningln(v ...interface{})           { l.eachv(Logger.Warningln, v...) }
