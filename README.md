# Go Logging

[![GoDoc](https://godoc.org/gitlab.com/go-utils/logging?status.svg)](http://godoc.org/gitlab.com/go-utils/logging)

`gitlab.com/go-utils/logging` provides common logging interfaces, polyfilling,
and a fan-out logger.