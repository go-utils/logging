/*
	Go Logging provides common logging interfaces.
	Copyright 2019 The Go Logging Authors.

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// polyfill.go: logging implementations to polyfill simple loggers

package logging

import (
	"fmt"
	"os"
)

type (
	polyFlagsLogger  struct{ b FatalLogger }
	polyFormatLogger struct{ b FatalLogger }
	polyDepthLogger  struct{ b FatalLogger }
	polyLineLogger   struct{ b FatalLogger }

	polyFatalLogger struct{ SimpleLogger }

	poly struct {
		FatalLogger
		FlagsLogger
		FormatLogger
		DepthLogger
		LineLogger
	}
)

func Polyfill(l SimpleLogger) Logger {
	if ll, ok := l.(Logger); ok {
		return ll
	}

	basic, ok := l.(FatalLogger)
	if !ok {
		basic = polyFatalLogger{l}
	}

	flags, ok := l.(FlagsLogger)
	if !ok {
		flags = polyFlagsLogger{basic}
	}

	format, ok := l.(FormatLogger)
	if !ok {
		format = polyFormatLogger{basic}
	}

	depth, ok := l.(DepthLogger)
	if !ok {
		depth = polyDepthLogger{basic}
	}

	line, ok := l.(LineLogger)
	if !ok {
		line = polyLineLogger{basic}
	}

	return poly{basic, flags, format, depth, line}
}

func (polyFlagsLogger) SetFlags(flag int) {}

func (p polyFatalLogger) Fatal(v ...interface{}) {
	p.SimpleLogger.Error(v)
	os.Exit(1)
}

func (p polyFormatLogger) Errorf(format string, v ...interface{})   { sf(p.b.Error, format, v...) }
func (p polyFormatLogger) Fatalf(format string, v ...interface{})   { sf(p.b.Fatal, format, v...) }
func (p polyFormatLogger) Infof(format string, v ...interface{})    { sf(p.b.Info, format, v...) }
func (p polyFormatLogger) Warningf(format string, v ...interface{}) { sf(p.b.Warning, format, v...) }
func (p polyDepthLogger) ErrorDepth(depth int, v ...interface{})    { p.b.Error(v...) }
func (p polyDepthLogger) FatalDepth(depth int, v ...interface{})    { p.b.Fatal(v...) }
func (p polyDepthLogger) InfoDepth(depth int, v ...interface{})     { p.b.Info(v...) }
func (p polyDepthLogger) WarningDepth(depth int, v ...interface{})  { p.b.Warning(v...) }
func (p polyLineLogger) Errorln(v ...interface{})                   { sln(p.b.Error, v...) }
func (p polyLineLogger) Fatalln(v ...interface{})                   { sln(p.b.Fatal, v...) }
func (p polyLineLogger) Infoln(v ...interface{})                    { sln(p.b.Info, v...) }
func (p polyLineLogger) Warningln(v ...interface{})                 { sln(p.b.Warning, v...) }

func sf(print func(...interface{}), format string, v ...interface{}) {
	print(fmt.Sprintf(format, v...))
}

func sln(print func(...interface{}), v ...interface{}) {
	print(fmt.Sprintln(v...))
}
