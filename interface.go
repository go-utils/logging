/*
	Go Logging provides common logging interfaces.
	Copyright 2019 The Go Logging Authors.

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// interface.go: logging interfaces

package logging

type Logger interface {
	FlagsLogger
	FatalLogger
	FormatLogger
	DepthLogger
	LineLogger
}

type FlagsLogger interface {
	SetFlags(flag int)
}

type SimpleLogger interface {
	Error(v ...interface{})
	Info(v ...interface{})
	Warning(v ...interface{})
}

type FatalLogger interface {
	SimpleLogger
	Fatal(v ...interface{})
}

type FormatLogger interface {
	Errorf(format string, v ...interface{})
	Fatalf(format string, v ...interface{})
	Infof(format string, v ...interface{})
	Warningf(format string, v ...interface{})
}

type DepthLogger interface {
	ErrorDepth(depth int, v ...interface{})
	FatalDepth(depth int, v ...interface{})
	InfoDepth(depth int, v ...interface{})
	WarningDepth(depth int, v ...interface{})
}

type LineLogger interface {
	Errorln(v ...interface{})
	Fatalln(v ...interface{})
	Infoln(v ...interface{})
	Warningln(v ...interface{})
}
